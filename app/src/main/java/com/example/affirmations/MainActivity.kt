/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.EventLogTags
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.ViewModel.MainViewModel
import com.example.affirmations.ui.theme.AffirmationsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationsTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    AffirmationApp(gotodetail = { it -> gotodetail(it)})
                }

            }
            // TODO 5. Show screen
        }
    }
    
    fun gotodetail(id:Int){
        val intent= Intent(this, DetailActivity::class.java)
        intent.putExtra("affirmation", id)
        this.startActivity(intent)
    }
}

@Composable
fun AffirmationApp(gotodetail:(Int)->Unit, viewModel: MainViewModel = viewModel()) {
    // TODO 4. Apply Theme and affirmation list
    viewModel.init()
    val uiState by viewModel.uiState.collectAsState()

    AffirmationsTheme(darkTheme = true) {
        uiState?.let {
            AffirmationList(affirmationList = it, gotodetail = gotodetail)
        }
    }
}

@Composable
fun AffirmationList(
    affirmationList: List<Affirmation>,
    modifier: Modifier = Modifier,
            gotodetail:(Int)->Unit
) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn() {
        this.items(
            items = affirmationList,
            itemContent = { item -> AffirmationCard(affirmation = item, gotodetail = gotodetail) })
    }
}

@Composable
fun AffirmationCard(
    affirmation: Affirmation, 
    modifier: Modifier = Modifier,
            gotodetail:(Int)->Unit
) {
    // TODO 1. Your card UI
    val expanded = remember { mutableStateOf(false) }
    Card(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 5.dp)
    ) {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
        ) {
            Row() {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = "image",
                    modifier = Modifier
                        .padding(10.dp)
                        .size(70.dp)
                        .clip(RoundedCornerShape(30)),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    textAlign = TextAlign.Justify,
                    fontSize = 20.sp,
                    modifier = Modifier
                        .padding(top = 5.dp, end = 2.dp)
                        .weight(1f)
                )
                AffirmationCardButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    }
                )
            }
            if (expanded.value) {
                Description(gotodetail=gotodetail, affirmation)
            }
        }
    }
}


@Composable
fun AffirmationCardButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.primary,
            contentDescription = "wetepops", modifier = Modifier.size(30.dp)
        )

    }

}

@Composable
fun Description(
    gotodetail:(Int)->Unit,
    affirmation: Affirmation
) {

    //val context = LocalContext.current as MainActivity
    
    Column(
        modifier = Modifier.padding(
            start = 16.dp,
            top = 8.dp,
            bottom = 16.dp,
            end = 16.dp
        )
    ) {
        Text(
            text = AnnotatedString("Description: "),
            style = MaterialTheme.typography.h6,
        )
        Text(
            text = AnnotatedString("Aquí me gustaría ir de vacaciones con mi gato y jugar al rugby con los patos y las vacas para ver quien gana muajajajajajaja"),
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis,
            maxLines = 2,
        )
        Button(onClick = {
            gotodetail(affirmation.affirmationID)
        }) {
            Text(text = "Ver más")
        }

    }
}
// TODO 2. Preview your card


