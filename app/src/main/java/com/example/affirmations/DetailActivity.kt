package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log.d
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.ViewModel.DetailViewModel
import com.example.affirmations.ui.theme.AffirmationsTheme

class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val id = intent.getIntExtra("affirmation", 0)
        setContent {
            AffirmationsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background
                ) { AffirmationDetail(id = id) }
            }
        }
    }
}

@Composable
fun AffirmationDetail(
    detailViewModel: DetailViewModel = viewModel(),
    id: Int,
) {
    val context = LocalContext.current
    detailViewModel.init(id)
    val uiState by detailViewModel.uiState.collectAsState()
    uiState?.let {
        Column(modifier = Modifier, horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                painter = painterResource(id = it.imageResourceId),
                contentDescription = "DetailImage",
                modifier = Modifier
                    .padding(20.dp)
                    .size(400.dp)
                    .clip(RoundedCornerShape(10)),
                contentScale = ContentScale.Crop
            )
            Arrangement.Center
            Text(
                text = stringResource(id = it.stringResourceId),
                textAlign = TextAlign.Justify,
                fontSize = 20.sp,
                modifier = Modifier
                    .padding(top = 5.dp, end = 2.dp)
                    .weight(1f)
            )
            Row() {
                IconButton(onClick = {context.sendMail(it.email, "Hey! Echale un ojo!") }) {
                    Icon(Icons.Filled.Email,
                    tint = MaterialTheme.colors.primary,
                    contentDescription="icono email", modifier = Modifier.size(30.dp)
                    )

                }
Spacer(modifier = Modifier.size(20.dp))
                IconButton(onClick = {context.dial(it.phonenumber)}){
                    Icon(Icons.Filled.Phone,
                    tint = MaterialTheme.colors.primary,
                    contentDescription = "phone icon", modifier = Modifier.size(30.dp))
                }
            }
        }
    }

}

private fun Context.sendMail(to: String, subject: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {

    } catch (t: Throwable) {

    }
}

private fun Context.dial(phone:String){
    try{
        val intent= Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    }catch (t:Throwable){

    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AffirmationsTheme {

    }
}